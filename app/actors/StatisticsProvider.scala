package actors

import actors.StatisticProvider.{ReviveStorage, ServiceUnavailable}
import akka.actor.SupervisorStrategy.{Escalate, Restart}
import akka.actor.{Actor, ActorLogging, ActorRef, OneForOneStrategy, Props, SupervisorStrategy, Terminated}
import messages.ComputeReach
import org.joda.time.{DateTime, Interval}
import play.api.Configuration
import play.api.libs.ws.WSClient
import reactivemongo.core.errors.ConnectionException

import scala.concurrent.ExecutionContext

class StatisticsProvider(ws: WSClient, config: Configuration) extends Actor with ActorLogging {

  var reachComputer: ActorRef = _
  var storage: ActorRef = _
  var followersCounter: ActorRef = _

  implicit val ec: ExecutionContext = context.dispatcher

  import scala.concurrent.duration._


  override def supervisorStrategy: SupervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 3, withinTimeRange = 2.minutes) {
      case _: ConnectionException => Restart // Reactive MongoDB problems...
      case t: Throwable => super.supervisorStrategy.decider.applyOrElse(t, (_: Any) => Escalate)
    }


  override def preStart(): Unit = {
    log.info("Starting statistic provider")
    followersCounter = context.actorOf(Props[UserFollowersCounter], name = "userFollowersCounter")
    storage = context.actorOf(Props[Storage], name = "storage")
    reachComputer = context.actorOf(TwitterReachComputer.props(ws, config, followersCounter, storage))
    context.watch(storage)
  }

  override def receive: Receive = {
    case reach: ComputeReach => reachComputer forward reach
    case Terminated(terminatedStorageRef) => context.system.scheduler.scheduleOnce(1.minute, self, ReviveStorage)
      context.become(storageUnavailable)
    case TwitterRateLimitReached(reset) =>
      context.system.scheduler.scheduleOnce(new Interval(DateTime.now, reset).toDurationMillis.millis, self, ResumeService)
      context.become(serviceUnavailable)
    case UserFollowersCounterUnavailable =>
      context.become(userFollowersCounterUnavailable)
  }

  def serviceUnavailable: Receive = {
    case reach @ ComputeReach(_) =>
      sender() ! ServiceUnavailable
    case ResumeService =>
      context.unbecome()
  }

  def storageUnavailable: Receive = {
    case ComputeReach(_) =>
      sender() ! ServiceUnavailable
    case ReviveStorage =>
      storage = context.actorOf(Props[Storage], name = "storage")
      context.unbecome()
  }

  def userFollowersCounterUnavailable: Receive = {
    case ComputeReach(_) =>
      sender() ! ServiceUnavailable
    case UserFollowersCounterAvailable =>
      context.unbecome()
  }
}

case object ResumeService

object StatisticProvider {
  case object ServiceUnavailable
  case object ReviveStorage
}

object StatisticsProvider {
  def props(ws: WSClient, config: Configuration): Props = Props(classOf[StatisticsProvider], ws, config)
}
