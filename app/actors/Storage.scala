package actors

import actors.Storage.LastStorageError
import akka.actor.{Actor, ActorLogging, ActorRef}
import messages.{ReachStored, StoreReach}
import org.joda.time.{DateTime, DateTimeZone}
import org.joda.time.format.ISODateTimeFormat
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.commands.{LastError, WriteResult}
import reactivemongo.api.{DefaultDB, MongoConnection, MongoDriver}
import reactivemongo.bson.{BSONBinary, BSONDateTime, BSONDocument, BSONDocumentReader, BSONDocumentWriter, BSONHandler, Macros, Subtype}
import reactivemongo.core.errors.ConnectionException

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

case class StoredReach(when: DateTime, tweetId: BigInt, score: Int)

object StoredReach {

  DateTimeZone.setDefault(DateTimeZone.UTC)

  implicit object BSONDateTimeHandler extends BSONHandler[BSONDateTime, DateTime] {
    val fmt = ISODateTimeFormat.dateTime()
    def read(time: BSONDateTime) = new DateTime(time.value)
    def write(jdtime: DateTime) = BSONDateTime(jdtime.getMillis)
  }

  implicit object BigIntHandler extends BSONDocumentReader[BigInt] with BSONDocumentWriter[BigInt] {
    def write(bigInt: BigInt): BSONDocument = BSONDocument(
      "signum" -> bigInt.signum,
      "value" -> BSONBinary(bigInt.toByteArray, Subtype.UserDefinedSubtype))

    def read(doc: BSONDocument): BigInt = BigInt(
      doc.getAs[Int]("signum").get, {
        val buf = doc.getAs[BSONBinary]("value").get.value
        buf.readArray(buf.readable())
      })
  }

  implicit object StoredReachHandler extends BSONDocumentReader[StoredReach] with BSONDocumentWriter[StoredReach] {
    override def read(bson: BSONDocument): StoredReach = {
      val when = bson.getAs[BSONDateTime]("when").map(t => new DateTime(t.value)).get
      val tweetId = bson.getAs[BigInt]("tweet_id").get
      val score = bson.getAs[Int]("score").get
      StoredReach(when, tweetId, score)
    }

    override def write(r: StoredReach): BSONDocument = BSONDocument(
      "when" -> BSONDateTime(r.when.getMillis),
      "tweetId" -> r.tweetId,
      "tweet_id" -> r.tweetId,
      "score" -> r.score
    )
  }

}

class Storage extends Actor with ActorLogging {
  val Database = "twitterService"
  val ReachCollection = "ComputedReach"

  import StoredReach._

  implicit val executionContext = context.dispatcher
  implicit def storedReachWriter: BSONDocumentWriter[StoredReach] = Macros.writer[StoredReach]

  val driver: MongoDriver = new MongoDriver()
  var connection: MongoConnection = _
  var db: Future[DefaultDB] = _
  var collection: BSONCollection = _

  obtainConnection()

  var currentWrites = Set.empty[BigInt]

  override def postRestart(reason: Throwable): Unit = {
    reason match {
      case ce: ConnectionException => obtainConnection()
    }
    super.postRestart(reason)
  }

  override def postStop(): Unit = {
    connection.close()
    driver.close()
  }

  import akka.pattern.pipe

  override def receive: Receive = {
    case StoreReach(tweetId, score) =>
      log.info("Storing reach for tweets {}", tweetId)
      if (!currentWrites.contains(tweetId)) {
        currentWrites = currentWrites + tweetId
        val originalSender = sender()
        collection
          .insert(StoredReach(DateTime.now, tweetId, score))
          .map { lastError =>
            LastStorageError(lastError, tweetId, originalSender)
          }.recover {
            case _ =>
              currentWrites = currentWrites - tweetId
          } pipeTo self
      }
    case LastStorageError(error, tweetId, sender) =>
      if (error.inError) {
        currentWrites = currentWrites - tweetId
      } else {
        sender ! ReachStored(tweetId)
      }
  }

  private def obtainConnection(): Unit = {
    connection = driver.connection(List("localhost"))
    db = connection.database(Database)
    collection = Await.result(db.map(_.collection[BSONCollection] (ReachCollection)), 10.seconds)
  }
}
object Storage {
  case class LastStorageError(error: WriteResult, tweetId: BigInt, client: ActorRef)
}

case class StoredReachException(storedReach: StoredReach) extends RuntimeException