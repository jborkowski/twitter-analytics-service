package actors

import akka.actor.{Actor, ActorLogging, ActorRef, Cancellable, Props}
import messages._
import play.api.Configuration
import play.api.libs.json.JsArray
import play.api.libs.oauth.OAuthCalculator
import play.api.libs.ws.WSClient

import scala.concurrent.Future
import scala.util.control.NonFatal

class TwitterReachComputer (ws: WSClient,
                            override val config: Configuration,
                            followersCounter: ActorRef,
                            storage: ActorRef) extends Actor with ActorLogging with TwitterCredentials {
  implicit val executionContext = context.dispatcher

  var followersCounterByRetweet = Map.empty[FetchedRetweets, List[FollowerCount]]

  import scala.concurrent.duration._
  val retryScheduler: Cancellable = context.system.scheduler.schedule(1.second, 20.second, self, ResendUnacknowledged)

  override def postStop(): Unit = {
    retryScheduler.cancel()
  }

  import akka.pattern.pipe

  override def receive: Receive = {
    case ComputeReach(tweetId) =>
      val originalSender = sender()
      fetchRetweets(tweetId, sender()).recover{
        case NonFatal(t) => RetweetFetchingFailed(tweetId, t, originalSender)
      } pipeTo self

    case fetchedRetweets: FetchedRetweets =>
      followersCounterByRetweet += fetchedRetweets -> List.empty
      fetchedRetweets.retweeters.foreach { rt =>
        followersCounter ! FetchFollowersCount(fetchedRetweets.tweetId, rt)
      }

    case count @ FollowerCount(tweetId, _, _) =>
      log.info("Received followers count for {}", tweetId)
      fetcherRetweetsFor(tweetId).foreach { fetchedRetweets =>
        updateFollowersCount(tweetId, fetchedRetweets, count)
      }

    case ReachStored(tweetId) =>
      followersCounterByRetweet.keys
        .find(_.tweetId == tweetId)
        .foreach { key =>
          followersCounterByRetweet.filterNot(_._1 == key)
        }

    case ResendUnacknowledged =>
      val unacknowledged = followersCounterByRetweet.filterNot {
        case (retweet, counts) =>
          retweet.retweeters.size != counts.size
      }
      unacknowledged.foreach { case (retweet, counts) =>
          val score = counts.map(_.followersCount).sum
          storage ! StoreReach(retweet.tweetId, score)
      }
  }

  case class FetchedRetweets(tweetId: BigInt, retweeters: List[String], client: ActorRef)

  case class RetweetFetchingFailed(tweetId: BigInt, cause: Throwable, client: ActorRef)

  def fetcherRetweetsFor(tweetId: BigInt) = followersCounterByRetweet.keys.find(_.tweetId == tweetId)

  def updateFollowersCount(tweeterId: BigInt, fetchedRetweets: FetchedRetweets, count: FollowerCount) = {
    val existingCount = followersCounterByRetweet(fetchedRetweets)

    followersCounterByRetweet = followersCounterByRetweet.updated(fetchedRetweets, count :: existingCount)

    val newCounts = followersCounterByRetweet(fetchedRetweets)
    if (newCounts.length == fetchedRetweets.retweeters.length) {
      log.info("Received all retweeters followers count for tweet {}, computing sum", tweeterId)
      val score = newCounts.map(_.followersCount).sum
      fetchedRetweets.client ! TweetReach(tweeterId, score)
      storage ! StoreReach(tweeterId, score)
    }
  }

  def fetchRetweets(tweetId: BigInt, client: ActorRef): Future[FetchedRetweets] = {
    credentials.map {
      case (consumerKey, requestToken) =>
        ws.url("https://api.twitter.com/1.1/statuses/retweeters/ids.json")
          .sign(OAuthCalculator(consumerKey,requestToken))
          .withQueryString("id" -> tweetId.toString)
          .withQueryString("stringify_ids" -> "true")
          .get().map { response =>
            if (response.status == 200) {
              val ids = (response.json \ "ids").as[JsArray].value.map(value => BigInt(value.as[String]).toString).toList
              FetchedRetweets(tweetId, ids, client)
            } else {
              throw new RuntimeException(s"Could not retrieve details for Tweet $tweetId")
            }
          }
    } getOrElse {
      Future.failed(new RuntimeException("You did not correctly configure Twitter credentials"))
    }
  }
}

object TwitterReachComputer {
  def props(ws: WSClient, config: Configuration, followersCounter: ActorRef, storage: ActorRef): Props =
    Props(classOf[TwitterReachComputer], ws, config, followersCounter, storage)
}

case object ResendUnacknowledged