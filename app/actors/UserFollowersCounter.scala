package actors

import akka.actor.{Actor, ActorLogging, Props}
import akka.dispatch.ControlMessage
import akka.pattern.CircuitBreaker
import messages.{FetchFollowersCount, FollowerCount}
import org.joda.time.DateTime
import play.api.Configuration
import play.api.libs.oauth.OAuthCalculator
import play.api.libs.ws.WSClient

import scala.concurrent.Future

class UserFollowersCounter(ws: WSClient,
                           override val config: Configuration) extends Actor with ActorLogging with TwitterCredentials {

  implicit val ec = context.dispatcher
  val remainingLimit = "X-Rate-Limit-Remaining"
  val resetLimit = "X-Rate-Limit-Reset"

  import scala.concurrent.duration._
  val breaker =
    new CircuitBreaker(context.system.scheduler, maxFailures = 5, callTimeout = 2.second, resetTimeout = 1.minute)


  import akka.pattern.pipe
  override def receive: Receive = {
    case FetchFollowersCount(tweetId, user) =>
      val originalSender = sender()
      breaker
        .onOpen({
          log.info("Circuit breaker open")
          originalSender ! FollowerCountUnavailable(tweetId, user)
          context.parent ! UserFollowersCounterUnavailable
        })
        .onHalfOpen({
          log.info("Circuit breaker half-open")
        })
        .onClose({
          log.info("Circuit breaker Close")
          context.parent ! UserFollowersCounterAvailable
        })
        .withCircuitBreaker(fetchFollowersCounts(tweetId, user)) pipeTo sender()
  }

  private def fetchFollowersCounts(tweetId: BigInt, userId: String): Future[FollowerCount] = {
    credentials.map { case (consumerKey, requestToken) =>
        ws
          .url("https://api.twitter.com/1.1/users/show.json")
          .sign(OAuthCalculator(consumerKey,requestToken))
          .withQueryString("user_id" -> userId)
          .get().map { response =>
            if (response == 200) {

              val limits = for {
                remaining <- response.header(remainingLimit)
                reset <- response.header(resetLimit)
              } yield (remaining.toInt, new DateTime(reset.toLong * 1000))

              limits.foreach { case (remaining, reset) =>
                log.info("Rate limit: {} requests remaining, window resets at {}", remaining, reset)
                if (remaining < 50) {
                  Thread.sleep(10000)
                }
                if (remaining < 10) {
                  context.parent ! TwitterRateLimitReached(reset)
                }
              }
              val count = (response.json \ "followers_count").as[Int]
              FollowerCount(tweetId, userId, count)

            } else {
              throw new RuntimeException(s"Could not retrieve followers count for user $userId")
            }
          }
    }.getOrElse {
      Future.failed(new RuntimeException("You did not correctly configure the Twitter credentials"))
    }
  }
}

object UserFollowersCounter {
  def props = Props[UserFollowersCounter]
}

case class TwitterRateLimitReached(reset: DateTime) extends ControlMessage
case class FollowerCountUnavailable(tweetId: BigInt, user: String)
case object UserFollowersCounterUnavailable extends ControlMessage
case object UserFollowersCounterAvailable extends ControlMessage
