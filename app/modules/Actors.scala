package modules

import actors.StatisticsProvider
import akka.actor.ActorSystem
import com.google.inject.{AbstractModule, Inject}
import play.api.Configuration
import play.api.libs.ws.WSClient

class Actors @Inject() (system: ActorSystem, ws: WSClient, config: Configuration) extends ApplicationActors {
  system.actorOf(
    props = StatisticsProvider.props.(ws, config).withDispatcher("control-aware-dispatcher"),
    name = "statisticProvider"
  )
}

trait ApplicationActors

class ActorsModule extends AbstractModule {
  override def configure(): Unit = {
    bind(classOf[ApplicationActors]).to(classOf[Actors]).asEagerSingleton
  }
}
