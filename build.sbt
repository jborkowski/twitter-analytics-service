name := "twitter-analytics-service"

organization := "pl.jborkowski"

version := "1.0.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

val reactiveMongoVer = "0.11.14"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test,
  "com.typesafe.akka" % "akka-actor_2.11" % "2.4.10",
  "com.typesafe.akka" % "akka-slf4j_2.11" % "2.4.10",
  "org.reactivemongo" %% "play2-reactivemongo" % reactiveMongoVer
)

